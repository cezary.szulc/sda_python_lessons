def izogram_check(word):
    list_of_words = list(word.lower())
    set_of_words = set(list_of_words)
    if len(list_of_words) == len(set_of_words):
        print(f'Your word ({word}) is an izogram')
    else:
        print(f'Your word ({word}) is NOT an izogram')


if __name__ == '__main__':
    izogram_check(input("Please provide a word: "))
