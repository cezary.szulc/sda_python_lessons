def count_letters(word):
    return_dict = {}
    for letter in list(word.lower()):
        tmp_counter = return_dict.get(letter, 0)
        return_dict[letter] = tmp_counter + 1

    return return_dict


if __name__ == '__main__':
    while True:
        input_word = input('Provide word: ')
        points = count_letters(input_word)
        print(f'Word "{input_word}": {points}')
        input_rerun = input('Do you want to rerun application [y]/n: ')
        if input_rerun != 'y':
            break
