import os
import platform
import sys
import pandas as pd


def print_system_info():
    print(os.name)
    print(platform.system())
    print(platform.release())
    print(sys.platform)
    print(pd.__version__)


if __name__ == '__main__':
    print_system_info()
