import random


def play_multilotek(my_numbers):
        computer_numbers = random.sample(range(1, 20), 10)
        corect_shots = list(set(my_numbers).intersection(set(computer_numbers)))

        return corect_shots, my_numbers, computer_numbers


print(play_multilotek(list(range(1, 11))))
