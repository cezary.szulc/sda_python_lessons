def simple_decorator(func):
    def wrapper(*args, **kwargs):
        print('Hello, what is your name')
        func(*args, **kwargs)
        print('Nice to meet you')
    return wrapper


def print_my_name(name):
    print(f'Hi, my name is {name}')


@simple_decorator
def print_my_name_vol2(name):
    print(f'Hi, my name is {name}')


def check_even_number(func):
    def wrapper(number):
    # def wrapper(*args, **kwargs):
    #     number = args[0]
        if number % 2:
            print('Odd number')
            number += 1
        else:
            print('Even number')
        return_value = func(number)
        # func(*args, **kwargs)
        print(f'New number is {return_value}')
    return wrapper


@check_even_number
def print_number(number):
    print(f'My number is {number}')
    number *= 2

    return number


if __name__ == '__main__':
    print_number(6)
    print()
    print_number(11)
    # simple_decorator(print_my_name)('Ala')
    # print()
    # print_my_name_vol2('Mieszko')
