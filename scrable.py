dict_alfa = {chr(letter): value for letter, value in zip(range(97, 123), range(1, 27))}


def calculate_points(word):
    return sum([dict_alfa[letter.lower()] for letter in list(word)])


if __name__ == '__main__':
    while True:
        input_word = input('Provide word: ')
        points = calculate_points(input_word)
        print(f'Word "{input_word}" has {points} points')
        input_rerun = input('Do you want to rerun application [y]/n: ')
        if input_rerun != 'y':
            break
