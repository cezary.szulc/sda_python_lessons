zarobki_miesieczne = ["1000zl", 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000]


def podatek_roczny(func, prog_dochodowy=85000):

    def wrapper(zarobki):
        try:
            roczna_pensja = func(zarobki)
        except ValueError:
            print('Nie można policzyc pensji, prosze podac wylacznie wartosci liczbowe')
            roczna_pensja = 0
        print(f'Twoje roczne zarobki: {roczna_pensja}')

        if roczna_pensja < prog_dochodowy:
            print(f'Twoj podatek dochodowy to {round(roczna_pensja*0.17, 2)}')
        else:
            podatek_pierwszy_prog = prog_dochodowy * 0.17
            podatek_drugi_prog = (roczna_pensja - prog_dochodowy) * 0.32
            podatek_laczny = podatek_pierwszy_prog + podatek_drugi_prog
            print(f'Twoj podatek dochodowy to {round(podatek_laczny, 2)}')

        return roczna_pensja
    return wrapper


@podatek_roczny
def zarobki_roczne(zarobki):
    roczna_pensja = 0
    for i in zarobki:
        roczna_pensja += int(i)

    return roczna_pensja


zarobki_roczne(zarobki_miesieczne)
