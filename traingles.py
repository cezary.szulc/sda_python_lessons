def single_triangle(n):
    for i, y in zip(range(1, n+1), range(1, 2*n, 2)):
        print(' ' * (n-i) + '*' * y)


def many_traingles(x, h):
    for i in range(1, h+1):
        print(('*' * i + ' ' * (h - i + 1))*x)


def romb(n):
    #               [1,2,3,...]    [1,3,5,7,...]
    for i, y in zip(range(1, n+1), range(1, 2*n, 2)):
        print(' ' * (n-i) + '*' * y)
    for i, y in zip(range(1, n), range(2*(n-1), 1, -2)):
        print(' ' * i + '*' * (y-1))


if __name__ == '__main__':
    # single_triangle(2)
    # print()
    # many_traingles(10, 3)
    romb(8)
