dict_conversation = {
    ('czesc', 'hej', 'siema'): 'czesc jak sie masz',
    ('dobrze', 'wspaniale', 'kapitalnie'): 'Idealnie, jak Ci mogę pomóc?',
    ('źle', 'słabo', 'okropnie'): 'Oj to szkoda, jak Ci mogę poprawić humor?',
    ('Kawał', 'coś śmiesznego'): 'Co robi 9 złotych w portfelu? Ledwo dycha'
}


def conversation_with_bot(human_input):
    for key, value in dict_conversation.items():
        if human_input.lower() in key:
            return value
    return 'Przykro mi nie mogę odpowiedzieć na to pytanie'


if __name__ == '__main__':
    while True:
        input_word = input('What do you want to say: ')
        machine_output = conversation_with_bot(input_word)
        print(machine_output)
        input_rerun = input('Do you want to continue conversation [y]/n: ')
        if input_rerun != 'y':
            break
