def oblicz_liczbe_fibonacciego(ilosc):
    p = 0
    i = 0
    j = 1
    while p < ilosc:
        k = i+j
        i = j
        j = k
        p = p+1
    return i


def number_fib(n):
    a, b = 0, 1
    for x in range(n):
        a, b = b, a + b
    return a


def Fibbonacci(liczby):
    fib = []
    for liczba in range(liczby + 1):
        if liczba == 0:
           fib.append(0)
        elif liczba == 1:
           fib.append(1)
        else:
            fib.append(fib[-1]+fib[-2])

    return fib[-1]


def fib(n):
    a, b = 0, 1
    for _ in range(n):
        yield a
        a, b = b, a + b


# for x in range(10):
#     print(list(fib(x)))


def sortowanie_babelkowe(lista):
    for i in range(len(lista)-1):
        for j in range(len(lista)-i-2, -1, -1):
            print(lista)
            if lista[j] > lista[j+1]:
                lista[j], lista[j+1] = lista[j+1], lista[j]
    # print(lista)
    return lista


print(sortowanie_babelkowe([5, 3, 7, 9, 1, 34, 0]))
