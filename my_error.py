class MyError(Exception):
    pass


try:
    raise MyError('My example error message')
except MyError as e:
    print(e, e.args, type(e))
