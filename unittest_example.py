import unittest


class SecondTest(unittest.TestCase):

    def setUp(self):
        print('setUp function')

    def tearDown(self):
        print('tearDown function \n')

    def test_01(self):
        print(1)
        self.assertEqual(2, 2)

    def test_02(self):
        print(2)
        self.assertEqual(3, 3)

    def test_random_03(self):
        print(3)
        self.assertEqual(3, 3)

    def test_04(self):
        with self.assertRaises(ValueError):
            int('d')

    def test_05(self):
        with self.assertRaisesRegex(ValueError, 'literal'):
            int('aa')

