class Car:
    acceleration = 10

    def __init__(self, registration_number):
        self.registration_number = registration_number
        self.in_motion = False
        self.speed = 0

    def print_info(self):
        print(f'acceleration is {self.acceleration}')
        print(f'acceleration is {Car.acceleration}')
        # print(f'registration_number is {self.registration_number}')
        # print(f'in_motion is {self.in_motion}')
        # print(f'speed is {self.speed}')
        print()

    def drive(self):
        self.in_motion = True

    def accelerate(self, acceleration):
        if self.in_motion:
            self.speed += acceleration
        else:
            print('Your param in_montion is set to False')
            print()

    def stop(self):
        self.in_motion = False
        self.speed = 0


audi = Car('w1')
volvo = Car('w2')
mercedes = Car('w3')

audi.print_info()
audi.acceleration = 100
audi.print_info()
